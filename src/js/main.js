
/***
 * GERAL
 ***/
$(function() {

    App.SetToggle();
    App.SetGaleria();
    App.SetBanner();
    App.SetCarouselMobile();
    App.SetHeaderFixed(80);
    App.SetimagesLoaded();
    App.SetMaskInput();
    App.SetInputError();
    App.SetFormSuccess();
    App.SetAnimatescroll(150);
    App.SetWow(200);

});