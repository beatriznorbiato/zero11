<?php

// -----------------------------------------------------------------------------
//  API Key Google Maps
// -----------------------------------------------------------------------------
$api_maps = '';


// -----------------------------------------------------------------------------
// Includes 
// -----------------------------------------------------------------------------
include_once "core/helpers.php";
include_once "core/functions-modules.php";
include_once "core/functions-cpt.php";
include_once "core/functions-taxonomy.php";
include_once "core/functions-contact-form-front-end.php";
include_once "core/functions-contact-form.php";

// Admin
include_once "core/functions-admin.php";


// -----------------------------------------------------------------------------
// Incluo algumas informações no contexto do Timber
// -----------------------------------------------------------------------------
function add_to_context($data) {

    // Páginas
    $data['home'] = Timber::get_post(2);

    // Formulários
    global $contato;
    $data['contato'] = $contato->render();


    // Return
    return $data;
}
add_filter('timber_context', 'add_to_context');


// -----------------------------------------------------------------------------
// Forms
// -----------------------------------------------------------------------------
$email = get_field('email_formulario', 'option');

// Contact Form construct
$contato = new Contact_Form( 'contato', $email );

// Fields
$contato->set_field( 
	'text', 
	'nome', 
	'Nome', 
	true, 
	array(
		'attributes' => array(
			'placeholder' => 'Nome'
		)
	)
);
$contato->set_field( 
	'email', 
	'email', 
	'E-mail', 
	true, 
	array(
		'attributes' => array(
			'placeholder' => 'E-mail'
		)
	)
);
$contato->set_field( 
	'text', 
	'telefone', 
	'Telefone', 
	true, 
	array(
		'attributes' => array(
			'placeholder' => 'Telefone',
			'class'       => 'telefone',
		)
	)
);
$contato->set_field( 
	'text', 
	'assunto', 
	'Assunto', 
	true, 
	array(
		'attributes' => array(
			'placeholder' => 'Assunto'
		)
	)
);
$contato->set_field( 
	'textarea', 
	'mensagem', 
	'Mensagem', 
	true, 
	array(
		'attributes' => array(
			'placeholder' => 'Mensagem'
		)
	)
);

// Button
$contato->set_button( 'submit', 'Enviar', 'Button');

// Informacoes para envio
$contato->set_subject( 'Contato - ZERO11' );
$contato->set_reply_to( 'email' );


// -----------------------------------------------------------------------------
// CPTs
// -----------------------------------------------------------------------------
$cpt_lpage = new Custom_Post_Type( 'Landing page', 'Page', 'landing_page' );

