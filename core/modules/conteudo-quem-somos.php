<?php

// -----------------------------------------------------------------------------
// Register
// -----------------------------------------------------------------------------
// Modulo
$acf_conteudo_quem_somos = new Module('acf_conteudo_quem_somos', 'Conteúdo');

// Fields
$acf_conteudo_quem_somos->set_field('image', '', 'banner_sobre',
    array (
        'key'          => 'field_banner_sobre',
        'label'        => 'Banner',
        'name'         => 'banner_sobre',
        'type'         => 'image',
        'required'     => 1,
        'save_format'  => 'url',
        'preview_size' => 'thumbnail',
        'library'      => 'uploadedTo',
    )
);

$acf_conteudo_quem_somos->set_field('text', '', 'titulo_sobre',
    array (
        'key'          => 'field_titulo',
        'label'        => 'Título',
        'name'         => 'titulo_sobre',
        'type'         => 'text',
    )
);

$acf_conteudo_quem_somos->set_field('image', '', 'texto_sobre',
    array (
        'key'          => 'field_texto_sobre',
        'label'        => 'Texto',
        'name'         => 'texto_sobre',
        'type'         => 'wysiwyg',
        'media_upload' => 1,
    )
);

// Locations
$acf_conteudo_quem_somos->set_location('page', 11);

// Register
register_field_group($acf_conteudo_quem_somos->arguments());