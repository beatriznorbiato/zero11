<?php

// -----------------------------------------------------------------------------
// Register
// -----------------------------------------------------------------------------
// Modulo
$acf_conteudo_home = new Module('acf_conteudo_home', 'Conteúdo');

// Fields
$acf_conteudo_home->set_field('text', '', 'sobre_texto',
    array (
        'key'          => 'field_sobre_texto',
        'label'        => 'Texto (Breve Quem Somos)',
        'name'         => 'sobre_texto',
        'type'         => 'textarea',
    )
);

$acf_conteudo_home->set_field('image', '', 'sobre_imagem',
    array (
        'key'          => 'field_sobre_imagem',
        'label'        => 'Imagem (Breve Quem Somos)',
        'name'         => 'sobre_imagem',
        'type'         => 'image',
        'required'     => 1,
        'save_format'  => 'url',
        'preview_size' => 'thumbnail',
        'library'      => 'uploadedTo',
    )
);

$acf_conteudo_home->set_field('image', '', 'imagem_destaque',
    array (
        'key'          => 'field_imagem_destaque',
        'label'        => 'Imagem Destaque',
        'name'         => 'imagem_destaque',
        'type'         => 'image',
        'required'     => 1,
        'save_format'  => 'url',
        'preview_size' => 'thumbnail',
        'library'      => 'uploadedTo',
    )
);

// Locations
$acf_conteudo_home->set_location('page', 2);

// Register
register_field_group($acf_conteudo_home->arguments());