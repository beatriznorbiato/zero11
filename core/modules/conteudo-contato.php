<?php

// -----------------------------------------------------------------------------
// Register
// -----------------------------------------------------------------------------
// Modulo
$acf_conteudo_contato = new Module('acf_conteudo_contato', 'Conteúdo');

// Fields
$acf_conteudo_contato->set_field('image', '', 'banner_contato',
    array (
        'key'          => 'field_banner_contato',
        'label'        => 'Banner',
        'name'         => 'banner_contato',
        'type'         => 'image',
        'required'     => 1,
        'save_format'  => 'url',
        'preview_size' => 'thumbnail',
        'library'      => 'uploadedTo',
    )
);

// Locations
$acf_conteudo_contato->set_location('page', 12);

// Register
register_field_group($acf_conteudo_contato->arguments());